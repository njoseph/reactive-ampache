package com.antoniotari.reactiveampacheapp;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.antoniotari.reactiveampache.models.Song;
import com.antoniotari.reactiveampache.utils.Log;
import com.antoniotari.reactiveampacheapp.managers.ConnectionManager;
import com.antoniotari.reactiveampacheapp.managers.PlayManager;
import com.antoniotari.reactiveampacheapp.managers.PreferencesManager;
import com.antoniotari.reactiveampacheapp.utils.AmpacheCache;
import com.antoniotari.reactiveampacheapp.utils.Utils;
import com.facebook.network.connectionclass.ConnectionQuality;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by antoniotari on 2017-03-22.
 */

public class SongSelectedListener implements OnClickListener {
    private List<Song> mSongList;
    private Song mSong;

    public SongSelectedListener(Song song, List<Song> songList) {
        mSong = song;
        mSongList = songList;
    }

    @Override
    public void onClick(final View v) {
        onSongClick(v, Utils.getWaitToast(v.getContext()));
    }

    public void onSongClick(View view, final Toast waitToast) {
        waitToast.show();
        // some times the play action freezes the ui for a few milliseconds
        // post delay the play action to allow the ripple effect to take place
        // it also gives some time to the toast to show
        new Handler(Looper.getMainLooper()).postDelayed( () -> {
//            String cachePath = getCachedSongPath(view.getContext(), mSong);
//            String songUrl = cachePath == null ? mSong.getUrl() : cachePath;
//            AudioSister.getInstance().playNew(songUrl, mSong.getArtist().getName() + " - " + mSong.getTitle());
            PlayManager.INSTANCE.playNew(mSong, true);
            PlayManager.INSTANCE.setSongs(mSongList);
            PlayManager.INSTANCE.setCurrentSong(mSong);

            if (PreferencesManager.INSTANCE.isCacheEnabled()) {
                cacheSong(view.getContext());
            }
        }, 400);
    }

    private String getCachedSongPath(Context context, Song song) {
        return AmpacheCache.INSTANCE.getCachedSongPath(context, song);
    }

    private void cacheSong(Context context) {
        new ConnectionManager().getConnectionQuality()
                .doOnNext(Log::blu)
                .flatMap(new Func1<ConnectionQuality, Observable<Song>>() {
                    @Override
                    public Observable<Song> call(final ConnectionQuality connectionQuality) {
                        if (connectionQuality == ConnectionQuality.EXCELLENT ||
                                connectionQuality == ConnectionQuality.GOOD||
                                connectionQuality == ConnectionQuality.MODERATE) {
                            return Observable.from(PlayManager.INSTANCE.getCurrentPlaylist().subList(PlayManager.INSTANCE.getCurrentPlaylist().indexOf(mSong),PlayManager.INSTANCE.getCurrentPlaylist().size()));
                        }
                        return Observable.from(new ArrayList<Song>());
                    }
                })
                .filter(song1 -> getCachedSongPath(context,song1)==null)
                .flatMap(songFromList -> AmpacheCache.INSTANCE.downloadSong(context,songFromList))
                .subscribe(Log::blu, Log::error);
    }
}
