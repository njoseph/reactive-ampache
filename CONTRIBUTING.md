# Contributing to Power Ampache

Thank you for deciding to help this project! If you have contributed to other open source projects before please note that some conventions here might be a bit different than what you have been used to. Reading this document will save you, other contributors and the developers time.


## Development Ideology

Truths which we believe to be self-evident:

1. **The answer is not more options.**  If you feel compelled to add a preference that's exposed to the user, it's very possible you've made a wrong turn somewhere.
1. **There are no power users.** The idea that some users "understand" concepts better than others has proven to be, for the most part, false. If anything, "power users" are more dangerous than the rest, and we should avoid exposing dangerous functionality to them.
1. **It's an asynchronous world.**  Be wary of anything that is anti-asynchronous.
1. **There is no such thing as time.** Protocol ideas that require synchronized clocks are doomed to failure.


## Translations

Please threat translations as a normal commit, if you're not a developer please contact me for the list of strings that has to be translated and email me your translations.


## Issues

### Useful bug reports
1. Please search both open and closed issues first to make sure your issue is not a duplicate.

### Issue tracker is for bugs
The main purpose of this issue tracker is to track bugs for the Android client. Relevant, concise and to the point comments that help to solve the issue are very welcome.

### Don't bump issues
Every time someone comments on an issue, GitHub sends email to [everyone who is watching] the repository. Thus bumping issues with :+1:s, _me toos_ or asking for updates just generate unnecessary email notifications. Moreover bumping an issue does not help solving it. Please be respectful to everyone's time and try to only comment when you have relevant new information to add.

### Open issues

#### If it's open it's tracked
Nobody has commented on your issue? Is there no milestone or person assigned to it? Don't worry, the developers read every issue and if it's open it means it's tracked and taken into account. It might just take time as other issues have higher priorities. And remember that this is an open source project: Anyone is encouraged to take an active role in fixing open issues.


## Pull requests

### Follow the Code Style Guidelines
Before submitting a pull request please check that your code adheres to the [Code style Guidelines](link to come).

### Submit only complete PRs and test them
Please do not submit pull requests that are still a work in progress. Pull requests should be ready for a merge when you submit them. Also please do not submit pull requests that you have not tested.

### Smaller is better
Please do not try to change too much at once. Big changes are less likely to be merged. If you are a first time contributor start with small and simple PRs to get to know the codebase.

### Merging can sometimes take a while
If your pull request follows all the advice above but still has not been merged it usually means the developers haven't simply had the time to review it yet. We understand that this might feel frustrating. We are sorry!


## How can I contribute?
Any one can help by
- advising new people about the guidelines of this project
- improving documentation 
- translating
- finding and marking duplicate issues
- trying to reproduce issues
- finding solutions to open issues and posting relevant findings as comments
- submitting pull requests
- testing other people's pull requests
- spreading the joy of Power Ampache to your friends and family
- donating money 

